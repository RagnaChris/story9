from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import SigninForm, SignupForm, ForgotForm
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.utils.crypto import get_random_string
from django.http import HttpResponseNotFound

# Create your views here.
def homepage(request):
	if request.method == 'POST':
		try:
			del request.session['user_login']
			redirect('homepage')
		except KeyError:
			pass
	else:
		try:
			user = request.session['user_login']
			return redirect('dashboard', username=user)
		except:
			pass
	return render(request, 'homepage.html');

def signin(request):
	if request.method == 'POST':
		form = SigninForm(request.POST)
		if form.is_valid():
			u = form.cleaned_data['username']
			p = form.cleaned_data['password']
			try:
				pwd = getattr(User.objects.get(username=u), 'password')
				if p != pwd:
					return render(request, 'signin.html', {'form': form, 'response': "Wrong Password!"})
			except:
				return render(request, 'signin.html', {'form': form, 'response': "Account not found!"})
			request.session['user_login'] = u
			return redirect('dashboard', username=u)
	else:
		form = SigninForm()
	return render(request, 'signin.html', {'form': form, 'response': ""})

def signup(request):	
	if request.method == 'POST':
		form = SignupForm(request.POST)
		if form.is_valid():
			f = form.cleaned_data['first']
			l = form.cleaned_data['last']
			u = form.cleaned_data['username']
			e = form.cleaned_data['email']
			p = form.cleaned_data['password']
			cp = form.cleaned_data['confirm']
			if p != cp:
				return render(request, 'signup.html', {'form': form, 'response': "Password not match!"})
			if User.objects.filter(username = u).count() > 0:
				return render(request, 'signup.html', {'form': form, 'response': "Username has been registered before!"})
			user = User(first_name=f, last_name=l, username=u, email=e, password=p)
			user.save()
			request.session['user_login'] = u
			return redirect('dashboard', username=u)
	else:
		form = SignupForm()
	return render(request, 'signup.html', {'form': form, 'response': ""})

def dashboard(request, username):
	try:
		user = request.session['user_login']
		return render(request, 'dashboard.html', {"username": username})
	except:
		return HttpResponseNotFound("404 Not Found")
	

def forgot(request):
	if request.method == 'POST':
		form = ForgotForm(request.POST)
		if form.is_valid():
			e = form.cleaned_data['email']
			try:
				user = User.objects.get(email=e)
				newpass = get_random_string(12)
				msg = Mail(
					subject='Forget Password',
					html_content='Use this temporary password to login.\n'+newpass,
					from_email='Story9@ppw.com',
					to_emails=[e],
				)
				user.password = newpass
				user.save()
				sg = SendGridAPIClient('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')
				sg.send(msg)
				return render(request, 'forgot.html', {'form': form, 'response': "Email has been sent!"})
			except:
				return render(request, 'forgot.html', {'form': form, 'response': "Account not found!"})
	else:
		form = ForgotForm()
	return render(request, 'forgot.html', {'form': form, 'response': ""})
