from django.test import TestCase, Client
from django.urls import resolve
from .views import *
import unittest

# Create your tests here.
class Story9Test(TestCase):
	def test_homepage_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_homepage_using_auth_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'homepage.html')

	def test_homepage_using_auth_func(self):
		found = resolve('/')
		self.assertEqual(found.func, homepage)

	def test_User_models(self):
		u = User(first_name="first", last_name="last", username="user", email="email@mail.com", password="1234ABCDE")
		u.save()
		counting = User.objects.count()
		self.assertEqual(counting, 1)

	def test_signin_is_exist(self):
		response = Client().get('/signin/')
		self.assertEqual(response.status_code, 200)

	def test_signin_using_signin_template(self):
		response = Client().get('/signin/')
		self.assertTemplateUsed(response, 'signin.html')

	def test_signin_using_signin_func(self):
		found = resolve('/signin/')
		self.assertEqual(found.func, signin)

	def test_signin_url_is_working(self):
		u = User(first_name="first", last_name="last", username="user", email="email@mail.com", password="1234ABCDE")
		u.save()
		response = Client().post('/signin/', {'username': 'dummy', 'password': 'dum'})
		self.assertEqual(response.status_code, 200)

	def test_signin_url_is_working_true(self):
		u = User(first_name="first", last_name="last", username="user", email="email@mail.com", password="1234ABCDE")
		u.save()
		response = Client().post('/signin/', {'username': 'user', 'password': '1234ABCDE'})
		self.assertEqual(response.status_code, 302)

	def test_signup_is_exist(self):
		response = Client().get('/signup/')
		self.assertEqual(response.status_code, 200)

	def test_signup_using_signup_template(self):
		response = Client().get('/signup/')
		self.assertTemplateUsed(response, 'signup.html')

	def test_signup_using_signup_func(self):
		found = resolve('/signup/')
		self.assertEqual(found.func, signup)

	def test_signup_url_is_working(self):
		response = Client().post('/signup/', {'first': 'dummy', 'last': 'dum', 'username': 'dum', 'email': 'dummy@dum.com', 'password': 'dummy', 'confirm': 'notdummy'})
		self.assertEqual(response.status_code, 200)

	def test_signup_url_is_working_true(self):
		response = Client().post('/signup/', {'first': 'dummy', 'last': 'dum', 'username': 'dum', 'email': 'dummy@dum.com', 'password': 'dummy', 'confirm': 'dummy'})
		self.assertEqual(response.status_code, 302)

	def test_forgot_is_exist(self):
		response = Client().get('/forgot/')
		self.assertEqual(response.status_code, 200)

	def test_forgot_using_forgot_template(self):
		response = Client().get('/forgot/')
		self.assertTemplateUsed(response, 'forgot.html')

	def test_forgot_using_forgot_func(self):
		found = resolve('/forgot/')
		self.assertEqual(found.func, forgot)

	def test_forgot_url_is_working(self):
		response = Client().post('/forgot/', {'email': 'dummy@dum.com'})
		self.assertEqual(response.status_code, 200)