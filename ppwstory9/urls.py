from django.urls import path
from . import views

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('signin/', views.signin, name='signin'),
    path('signup/', views.signup, name='signup'),
    path('forgot/', views.forgot, name='forgot'),
    path('<str:username>/dashboard/', views.dashboard, name='dashboard'),
]