from django import forms
from django.forms import widgets

class ForgotForm(forms.Form):
	email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={"class": "right"}))

class SigninForm(forms.Form):
	username = forms.CharField(label="Username", widget=forms.TextInput(attrs={"class": "right"}))
	password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}))

class SignupForm(forms.Form):
	first = forms.CharField(label="First Name", widget=forms.TextInput(attrs={"class": "right"}), max_length=30)
	last = forms.CharField(label="Last Name", widget=forms.TextInput(attrs={"class": "right"}), max_length=30)
	username = forms.CharField(label="Username", widget=forms.TextInput(attrs={"class": "right"}), max_length=15)
	email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={"class": "right"}))
	password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}))
	confirm = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}), max_length=15)